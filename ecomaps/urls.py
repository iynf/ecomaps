from django.conf.urls import include, url, i18n, static
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import urls as auth_urls

from core import urls as core_urls
from maps import urls as maps_urls
from educational import urls as educational_urls


urlpatterns = [
    url(r'^djadmin/', include(admin.site.urls)),
    url(r'^', include(auth_urls)),
    url(r'^learn/', include(educational_urls)),
    url(r'^', include(maps_urls)),
    url(r'^', include(core_urls)),
]

urlpatterns = i18n.i18n_patterns(*urlpatterns)

if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]
