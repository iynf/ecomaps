from os import environ, path, listdir


def get_env_setting(setting):
    """ Get the environment setting or return exception """
    from django.core.exceptions import ImproperlyConfigured
    try:
        return environ[setting]
    except KeyError:
        error_msg = "Set the %s environment variable" % setting
        raise ImproperlyConfigured(error_msg)


PROJECT_DIR = path.dirname(path.dirname(path.abspath(__file__)))
BASE_DIR = path.dirname(PROJECT_DIR)


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'django_extensions',
    'django_countries',
    'django_cleanup',
    'django_comments',
    'phonenumber_field',
    'djangobower',
    'compressor',
    'registration',
    'ckeditor',
    'ckeditor_uploader',

    'core',
    'educational',
    'maps',
)

MIDDLEWARE_CLASSES = [
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',

    'dnt.middleware.DoNotTrackMiddleware',

    'core.middleware.VisitorIpMiddleware',
    'core.middleware.GeoipMiddleware',
]

ROOT_URLCONF = 'ecomaps.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'core.context_processors.staging',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ecomaps',
    }
}


AUTH_USER_MODEL = 'auth.User'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = 'auth_login'
LOGOUT_URL = 'auth_logout'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

USE_I18N = True
USE_L10N = True
USE_TZ = True
TIME_ZONE = 'Europe/Brussels'
LANGUAGE_CODE = 'en-us'

LANGUAGES = (
    ('cs', ("Czech")),
    ('de', ("German")),
    ('el', ("Greek")),
    ('en', ("English")),
    ('es', ("Spanish")),
    ('eo', ("Esperanto")),
    ('fr', ("French")),
    ('it', ("Italian")),
    ('lv', ("Latvian")),
    ('hu', ("Hungarian")),
    ('nl', ("Dutch")),
    ('pt', ("Portuguese")),
)


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

WWW_DIR = path.join(path.dirname(BASE_DIR), 'www')
STATIC_ROOT = path.join(WWW_DIR, 'static')
MEDIA_ROOT = path.join(WWW_DIR, 'media')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
)

ADMINS = (
    ('Baptiste Darthenay', 'baptiste.darthenay@gmail.com'),
)
MANAGERS = ADMINS
EMAIL_SUBJECT_PREFIX = '[ECOmaps] '
SERVER_EMAIL = 'ECOmaps 500 errors <errors@ecomaps.eu>'
DEFAULT_FROM_EMAIL = 'Henrique from ECOmaps <support@ecomaps.eu>'

STAGING = False

BACKGROUNDS_DIR = 'img/backgrounds/'
BACKGROUNDS = listdir(path.join(BASE_DIR, 'core', 'static', BACKGROUNDS_DIR))


# Third party configuration

OPENCAGE_KEY = 'e3d1885ee2f4fcefd6de83a551791bb1'
OPENCAGE_URL = 'http://api.opencagedata.com/geocode/v1/json'

MAXMIND_COUNTRY_PATH = '/opt/maxmind/GeoLite2-Country.mmdb'
MAXMIND_CITY_PATH = '/opt/maxmind/GeoLite2-City.mmdb'


# Apps configuration

COMMENT_MAX_LENGTH = 500

CKEDITOR_UPLOAD_PATH = "educational/pictures/"

COUNTRIES_ONLY = [
    "AD", "AL", "AT", "AZ", "BA", "BE", "BG", "BY", "CH", "CY", "CZ",
    "DE", "DK", "EE", "ES", "FI", "FO", "FR", "GG", "GI", "GR",
    "HR", "HU", "IE", "IM", "IS", "IT", "JE", "LI", "LT", "LU",
    "LV", "MC", "MD", "MK", "MT", "NL", "NO", "PL", "PT", "RO",
    "RS", "RU", "SE", "SI", "SJ", "SK", "SM", "TR", "UA", "GB", "VA",
]

COMPRESS_ENABLED = True

BOWER_INSTALLED_APPS = (
    'jquery#2.1.4',
    'leaflet#0.7.7',
    'leaflet.draw',
    'materialize#0.97.5',
    'select2',
    'uri.js',
    'devbridge-autocomplete',
)

CKEDITOR_CONFIGS = {
    'default': {
        'skin': 'moono',
        'defaultLanguage': 'en',
        'toolbar_ECOmaps': [
            {'name': 'clipboard', 'items': ['Cut', 'Copy', 'PasteText', '-', 'Undo', 'Redo']},
            # {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'links', 'items': ['Link', 'Unlink']},
            {'name': 'insert', 'items': ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
            {'name': 'document', 'items': ['ShowBlocks', '-', 'Source']},
            {'name': 'tools', 'items': ['Maximize']},
            '/',
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Blockquote', 'CreateDiv', '-',
                       'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',
                       'Language']},
        ],
        'toolbar': 'ECOmaps',  # put selected toolbar config here
        'tabSpaces': 4,
        'extraPlugins': ','.join([
            'div',
            'autolink',
            'autoembed',
            'embedsemantic',
            'autogrow',
            'widget',
            'lineutils',
            'clipboard',
            'dialog',
            'dialogui',
            'elementspath'
        ]),
    }
}
