from .base import *


SITE_ID = 1
SECRET_KEY = get_env_setting('SECRET_KEY')

ALLOWED_HOSTS = ['ecomaps.eu', 'localhost', '[::1]']
WSGI_APPLICATION = 'ecomaps.wsgi.application'


EMAIL_HOST = 'mail.gandi.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_HOST_USER = 'errors@ecomaps.eu'
EMAIL_HOST_PASSWORD = get_env_setting('EMAIL_HOST_PASSWORD')
