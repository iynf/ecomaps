from .base import *


STAGING = True
SITE_ID = 2
SECRET_KEY = get_env_setting('SECRET_KEY')

ALLOWED_HOSTS = ['test.ecomaps.eu', 'localhost', '[::1]']
WSGI_APPLICATION = 'ecomaps.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'staging',
    }
}

EMAIL_SUBJECT_PREFIX = '[ECOmaps staging] '

EMAIL_HOST = 'mail.gandi.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'errors@ecomaps.eu'
EMAIL_HOST_PASSWORD = get_env_setting('EMAIL_HOST_PASSWORD')
