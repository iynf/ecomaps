from .base import *

SITE_ID = 1

DEBUG = True
COMPRESS_ENABLED = not DEBUG

SECRET_KEY = 'Not_So_Secret_Key'

ALLOWED_HOSTS = ['localhost', '[::1]']

INSTALLED_APPS += (
    'debug_toolbar',
)

if 'debug_toolbar' in INSTALLED_APPS:
    MIDDLEWARE_CLASSES.insert(
        1, 'debug_toolbar.middleware.DebugToolbarMiddleware'
    )

EMAIL_HOST = '127.0.0.1'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 1025  # MailDump
