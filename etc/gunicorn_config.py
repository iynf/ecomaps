command = '/opt/envs/prod/bin/gunicorn'
raw_env = ['DJANGO_SETTINGS_MODULE=ecomaps.settings.prod']
pythonpath = '/home/iynf/prod/ecomaps'
bind = ['127.0.0.1:8010', '[::1]:8010']
workers = 3
user = 'iynf'
timeout = 300
