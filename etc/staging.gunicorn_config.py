command = '/opt/envs/staging/bin/gunicorn'
raw_env = ['DJANGO_SETTINGS_MODULE=ecomaps.settings.staging']
pythonpath = '/home/iynf/staging/ecomaps'
bind = ['127.0.0.1:8020', '[::1]:8020']
workers = 3
user = 'iynf'
