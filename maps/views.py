import geojson

from django.shortcuts import get_object_or_404

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from core.utils import qstr, round_half, star_to_list
from core.mixins import JsonResponseMixin
from .models import City, Place, Rating
from .forms import PlaceForm, CityForm, RatingForm
from .choices import CATEGORIES


class GeoJsonListView(JsonResponseMixin, generic.list.BaseListView):
    def get_queryset(self):
        if self.kwargs.get('pk'):
            places = [Place.objects.get(id=self.kwargs['pk'])]
        elif self.kwargs.get('city') in City.objects.ids():
            places = Place.objects.filter(city=self.kwargs.pop('city'))
        else:
            places = Place.objects.all()

        categories = dict(self.request.GET).get('cat')
        if categories:
            places = places.filter(category__in=categories)

        search = self.request.GET.get('q')
        if search:
            places = places.filter(
                Q(name__icontains=search)
                | Q(description__icontains=search)
                | Q(locality__icontains=search)
            )

        return places

    def get_data(self, context):
        return geojson.FeatureCollection(
            [obj.feature for obj in context['object_list']]
        )


class BboxView(JsonResponseMixin, generic.View):
    http_method_names = [u'get']

    def get(self, request, *args, **kwargs):
        return self.render_to_response(context=self.kwargs)

    def get_data(self, context):
        city = get_object_or_404(City, id=context.get('city'))
        return city.bounds


class CitiesAutocompleteView(JsonResponseMixin, generic.View):
    http_method_names = [u'get']

    def get(self, request, *args, **kwargs):
        return self.render_to_response(context=self.kwargs)

    def get_data(self, context):
        suggestions = [{'data': data, 'value': value}
            for data, value in City.objects.values_with_country()]
        return {
            'query': 'Unit',
            'suggestions': suggestions,
        }


class RatingView(LoginRequiredMixin, JsonResponseMixin, generic.CreateView):
    model = Rating
    form_class = RatingForm
    raise_exception = True

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response(self.object)

    def form_invalid(self, form):
        return self.render_to_response(form)

    def get_data(self, data):
        if isinstance(data, Rating):
            rating = data
            return {
                'rating': rating.rating,
                'stars': rating.place.stars,
                'url': reverse('rating_detail', kwargs={'rating': rating.place.stars}),
            }
        return {'error': data.errors}


class RatingDetailView(generic.TemplateView):
    template_name = 'maps/rating_detail.html'

    def get(self, request, *args, **kwargs):
        self.rating = round_half(float(kwargs['rating']))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stars'] = star_to_list(self.rating)
        return context

class Map(generic.TemplateView):
    template_name = 'map.html'

    def get(self, request, *args, **kwargs):
        """ Move city from query string to URL. """
        if 'city' in request.GET:
            query_string = dict(request.GET)
            self.city_id = query_string.pop('city')[0]
            if self.city_id:
                url = reverse('map', kwargs={'city': self.city_id}) + qstr(query_string)
            else:
                url = reverse('map') + qstr(query_string)
            return redirect(url)
        if 'city' in kwargs:
            self.city_id = kwargs.get('city')
        return super().get(request, *args, **kwargs)

    def city(self):
        return City.objects.get(id=self.city_id)

    def categories(self):
        return CATEGORIES


class New(generic.CreateView):
    model = Place
    form_class = PlaceForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        if self.request.GET.get('city'):
            kwargs['city'] = self.request.GET.get('city')
        return kwargs


class Update(generic.UpdateView):
    model = Place
    form_class = PlaceForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class Detail(generic.DetailView):
    template_name = 'maps/place_detail.html'
    model = Place

    def stars(self):
        return star_to_list(self.object.stars)


class CityNew(generic.CreateView):
    model = City
    form_class = CityForm


class CityList(generic.ListView):
    model = City
    ordering = 'country'


class CityUpdate(generic.UpdateView):
    model = City
    form_class = CityForm
