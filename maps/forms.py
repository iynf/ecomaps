from geojson import Point

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify

from .models import City, Place, Tag, Rating
from .form_fields import TagField


class RatingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Rating
        fields = ('place', 'rating')

    def clean_rating(self):
        rating = self.cleaned_data['rating']
        if rating not in range(1, 6):
            raise forms.ValidationError("Out of rating range.")
        return rating

    def save(self):
        rating, created = Rating.objects.update_or_create(
            place=self.cleaned_data['place'],
            user=self.request.user,
            defaults={'rating': self.cleaned_data['rating']})
        return rating


class CityForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['country'].widget.attrs['class'] = ' browser-default'

    class Meta:
        model = City
        fields = ('name', 'country')


class PlaceForm(forms.ModelForm):
    lat = forms.FloatField(label=_("Latitude"))
    lng = forms.FloatField(label=_("Longitude"))
    tags = TagField(queryset=Tag.objects.used(), required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.new = bool(kwargs['instance'])
        city = kwargs.pop('city', None)
        super().__init__(*args, **kwargs)
        if city:
            self.initial['city'] = city
        if self.instance.geom:
            coord = self.instance.geom['coordinates']
            self.initial['lng'], self.initial['lat'] = coord[0], coord[1]
        for name, field in self.fields.items():
            field.widget.attrs['class'] = 'validate'
            if field.widget.__class__.__name__ == 'Textarea':
                field.widget.attrs['class'] += ' materialize-textarea'
            if name == 'category':
                field.widget.attrs['class'] += ' browser-default'
            if field.required:
                field.widget.attrs['required'] = True
        for name, errors in self.errors.items():
            self.fields[name].widget.attrs['class'] += ' invalid card-panel'

    class Meta:
        model = Place
        fields = (
            'name',
            'city',
            'category',
            'price',
            'description',
            'weloveit',
            'picture',
            'tags',
            'website',
            'facebook',
            'twitter',
            'email',
            'phone_number',
            'address',
            'locality',
            'postcode',
            'lat',
            'lng',
        )

    def save(self, commit=True):
        place = super().save(commit=False)
        if not self.new:
            place.created_by = self.user
        place.modified_by = self.user
        lat = self.cleaned_data['lat']
        lng = self.cleaned_data['lng']
        place.geom = Point([lng, lat])
        place.slug = slugify(self.cleaned_data['name'])
        if commit:
            place.save()
            self.save_m2m()
        return place
