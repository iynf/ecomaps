from geojson import Feature
import requests

from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.template import loader
from django.core.urlresolvers import reverse
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from core.utils import round_half, star_to_list
from .fields import PointField
from .choices import CATEGORY_CHOICES, PRICE_CHOICES, get_cat
from .managers import TagManager, CityQuerySet, PlaceManager
TimeStampedModel = models.Model


class Tag(models.Model):
    name = models.CharField(_("name"), unique=True, max_length=42)
    slug = models.SlugField(_("slug"), unique=True)

    objects = TagManager()

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    def __str__(self):
        return self.name


class Rating(models.Model):
    place = models.ForeignKey('maps.Place')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    rating = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")

    def __str__(self):
        return str(self.place.pk) + '*' * self.rating + self.user.username


class Place(TimeStampedModel):
    slug = models.SlugField()
    geom = PointField()
    name = models.CharField(verbose_name=_("name"), max_length=255)
    category = models.CharField(verbose_name=_("category"), max_length=25,
        choices=CATEGORY_CHOICES)
    tags = models.ManyToManyField('maps.Tag', verbose_name=_('tags'))
    description = models.TextField(_("description"), blank=True)
    price = models.PositiveSmallIntegerField(_("price"),
        choices=PRICE_CHOICES, default=0)
    city = models.ForeignKey('City', related_name='places')
    address = models.CharField(verbose_name=_("address"), max_length=255)
    locality = models.CharField(verbose_name=_("locality"), max_length=255)
    postcode = models.CharField(verbose_name=_("postcode"), max_length=12)
    phone_number = PhoneNumberField(verbose_name=_("phone number"), blank=True)
    email = models.EmailField(verbose_name=_("email"), blank=True)
    website = models.URLField(verbose_name=_("website"), blank=True)
    facebook = models.URLField(verbose_name=_("facebook"), blank=True)
    twitter = models.URLField(verbose_name=_("twitter"), blank=True)
    weloveit = models.CharField(verbose_name=_("why we love it"), blank=True,
        max_length=500, default="")
    picture = models.ImageField(_("picture"), upload_to='places', blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='places')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modified')
    raters = models.ManyToManyField(settings.AUTH_USER_MODEL, through='Rating', related_name="rated_places")

    objects = PlaceManager()

    class Meta:
        verbose_name = _("Place")
        verbose_name_plural = _("Places")

    def __str__(self):
        return self.name

    def price_symbol(self):
        symbol = "€"
        return " ".join(symbol * self.price)

    def phone(self):
        if not self.phone_number:
            return ''
        return self.phone_number.as_international

    @property
    def popupContent(self):
        template = loader.get_template('maps/popup.html')
        return template.render({'place': self})

    @property
    def feature(self):
        return Feature(
            geometry=self.geom,
            properties={
                'popupContent': self.popupContent,
                'color': get_cat(self.category).color}
        )

    @property
    def stars(self):
        avg = self.rating_set.aggregate(models.Avg('rating'))['rating__avg']
        return round_half(avg)

    @property
    def star_list(self):
        return star_to_list(self.stars)

    def get_absolute_url(self):
        return reverse('detail', kwargs={'pk': self.id, 'slug': self.slug})


class City(TimeStampedModel):
    id = models.SlugField(primary_key=True, null=False, unique=True)
    name = models.CharField(verbose_name=_("name"), max_length=255)
    country = CountryField(verbose_name=_("country"), blank_label=_("Country"))
    bounds = JSONField(default=dict, blank=True)

    objects = CityQuerySet.as_manager()

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")

    def __str__(self):
        return self.name

    def request_bounds(self):
        params = {
            'url': settings.OPENCAGE_URL,
            'key': settings.OPENCAGE_KEY,
            'query': self.id,
        }
        url = '{url}?q={query}&key={key}'.format(**params)
        response = requests.get(url)
        if response.ok:
            data = response.json()
            if data['total_results']:
                return data['results'][0]['bounds']
        return dict()

    def get_absolute_url(self):
        return reverse('city', kwargs={'pk': self.id})

    def save(self, *args, **kwargs):
        if not self.id:
            self.id = slugify(self.name)
        if not self.bounds:
            self.bounds = self.request_bounds()
        super().save(*args, **kwargs)
