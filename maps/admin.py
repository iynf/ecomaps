from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import City, Place, Tag


class PopularityFilter(admin.SimpleListFilter):
    title = _("Popularity")
    parameter_name = 'popularity'

    def lookups(self, request, model_admin):
        choices = (
            (0, _("Unused")),
            (10, _("Most popular (10+)")),
        )
        return (choices + tuple((i, i) for i in range(9, 0, -1)))

    def queryset(self, request, queryset):
        if self.value():
            value = int(self.value())
            return queryset.filter(popularity__in=[value, value])
        return queryset


class AlphabetFilter(admin.SimpleListFilter):
    title = _("Letter")
    parameter_name = 'letter'

    def lookups(self, request, model_admin):
        names = model_admin.get_queryset(request).values_list('name', flat=True)
        letters = sorted({name[:1].upper() for name in names})
        return ((l, l) for l in letters)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(name__istartswith=self.value())
        return queryset


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    prepopulated_fields = {'id': ('name',)}


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'popularity')
    list_filter = (PopularityFilter, AlphabetFilter)

    def popularity(self, obj):
        return obj.popularity


class TaggedInline(admin.TabularInline):
    model = Place.tags.through
    extra = 1
    verbose_name = Tag._meta.verbose_name
    verbose_name_plural = Tag._meta.verbose_name_plural


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'category', 'city', 'address', 'locality', 'postcode', 'created_by')
    list_filter = ('city', 'category')
    search_fields = ('name', 'description', 'address', 'locality', 'created_by__username')
    inlines = [TaggedInline]
    fieldsets = (
        (None, {
            'fields': (
                ('name', 'slug'),
                'city',
                ('category', 'price'),
                'description',
                'weloveit',
                'picture',
            )
        }),
        (_("Address"), {
            'fields': (
                'address',
                'locality',
                'postcode',
            ),
            'classes': ['collapse']
        }),
        (_("Contact"), {
            'fields': (
                'phone_number',
                'email',
                'website',
                'facebook',
                'twitter',
            ),
            'classes': ['collapse']
        }),
        (_("Position"), {
            'fields': (
                'geom',
            ),
            'classes': ['collapse']
        }),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.modified_by = request.user
        obj.save()

admin.site.site_header = 'ECOmaps'
