from django.db import models


class TagManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            popularity=models.Count('place')
        ).order_by('-popularity', 'name')

    def used(self):
        return self.get_queryset().filter(popularity__gt=0)

    def unused(self):
        return self.get_queryset().filter(popularity=0)


class PlaceManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(models.Avg('rating__rating'))


class CityQuerySet(models.QuerySet):
    def all(self):
        return super().all().order_by('id')

    def ids(self):
        return self.all().values_list('id', flat=True)

    def choices(self, label=None):
        choices = list(self.all().values_list('id', 'name'))
        return [('', label)] + choices if label else choices

    def values_with_country(self):
        return (
            (city.id,
            ", ".join([city.name, city.get_country_display()]))
            for city in self.all()
        )
