��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �      �       
   !     ,     @     X  
   i     t     {     �  
   �     �     �     �     �     �     �     �     �     �     �     �          6     J     P     W     _     e       	   �     �     �     �     �     �     �     �                    (     -  )   B     l     s     y     �  	   �     �     �     �     �     �  	   �     �            #        @     O     U     k  
   s  
   ~     �     �     �     �     �     �  	   �     �     �     �               $     +  	   :     D     L     X            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 tiendas de 2ª mano Añadir una nueva ciudad Añadir una imagen a este lugar! Añade este lugar! Dirección cultura alternativa Volver al mapa %(city)s Cuidados y Salud Categoría Niños Ciudades Ciudad Cafetería Contacto País Crear Comer y beber empresas ecológicas Editar Costoso Moda Completar para mí! Filtros tiendas de alimentos y mercados Geometría como GeoJSON Productos del hogar Hogar Barato Latitud Carta Estilo de vida y  familia Longitud Movilidad Movilidad y Turismo Moderado Más popular (10+) ONGs Natura 2000 senderos de naturaleza casas de Naturefriends  Nueva ciudad Nuevo lugar No aplicable Otro Parques y Naturaleza infraestructura para peatones y ciclistas Imagen Lugar Lugares Popularidad Posición Valorar este lugar: Clasificación Puntuaciones Puntos de reciclaje Etiqueta Etiquetas Turismo y Recreación No usado Actualización Restaurantes vegetarianos y veganos Ver en el mapa WWOOF ¿Por qué nos gusta? Trabajo Dirección categoría país descripción correo electrónico Facebook estrella llena media estrella Localidad nombre ninguna estrella número de teléfono imagen código postal precio URL semántica etiquetas twitter página web Por qué nos encanta 