��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �     �          &     -     D     b  
   w     �  	   �     �  
   �     �     �     �     �     �  	   �     �     �     �               9     Q     j     {     �     �     �     �  
   �     �     �     �     �     �          #     8     G     S     d     i  '   w     �     �     �     �     �     �     �     �  !   �          $     -     A     M  !   V     x     �     �     �     �  
   �     �     �     �     �     �     �     �                    &     .     <     A     E     N     V     b            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Lietoto apģērbu veikali Pievienot jaunu pilsētu Pievieno attēlu šai vietai! Pievienot šo vietu! Adrese Alternatīvā kultūra Atgriezties uz %(city)s karti Aprūpe un veselība Kategorija Bērni pilsētas pilsēta Kafejnīca Kontakti Valsts Izveidot Ēdieni un dzērieni Eko uzņēmumi Rediģēt Dārgs Mode Aizpildiet manā vietā! Filtri Pārtikas veikali un tirdziņi Ģeometrija kā GeoJSON Mājsaimniecības preces Mājsaimniecība Lēts Platums Vēstule Dzīvesstils & Ģimene Garums Kustīgums Mobilitāte un tūrisms Vidēji dārgs Populārākais (10+) NVO Natura 2000 "Natura trails" dabas takas Naturefriends mājas Jauna pilsēta Jauna vieta Nav piemērojams Cits Parki un daba Gājēju un velosipēdu infrastruktūra Attēls Vieta Vietas Popularitāte Amats Novērtēt šo vietu: Novērtējums Novērtējumi Otrreizējās pārstrādes punkti Atzīme Atzīmes Tūrisms un atpūta Neizmantots Atjaunot Veģetārie un vegānu restorāni Skatīt kartē WWOOF Kāpēc mums tas patīk? Darbs adrese kategorija valsts apraksts e-pasts facebook Zvaigzne puse no zvaigznes rajons vārds bez zvaigznes telefona numurs attēls pasta indekss cena URL Atzīmes twitter mājas lapa kāpēc mums tas patīk 