��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �  !   �          '     /     C     _  
   o     z     �     �     �     �     �     �     �     �     �     �     �     �     �  !   �     !     >     E     K     X     a     h  	     	   �     �     �     �     �     �     �     �                    .     4  '   D     l     r     w     }     �     �     �     �     �  
   �     �     �  
           *        E     W     ]     q     y  
   �     �     �     �     �  
   �  
   �  	   �     �     �     �     �                          $     ,     :            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Magasins de seconde main Ajouter une nouvelle ville Ajouter une photo à cet endroit! Ajouter ce lieu! Adresse Culture alternative Retour à la carte %(city)s Soins et santé Catégorie Enfants Villes Ville Café Contact Pays Créer Manger et boire entreprises Eco Éditer Coûteux Mode Remplissez-le pour moi! Filtres Magasins et marchés alimentaires La géométrie comme GeoJSON Maison Foyer Peu coûteux Latitude Lettre Mode de vie et famille Longitude Mobilité Mobilité & Tourisme Modérer Les plus populaires (10+) ONGs Natura 2000 Natura trails Maisons de Naturefriends Nouvelle ville Nouveau lieu Non applicable Autre Parcs et Nature Infrastructures pour piétons et cycles Image Lieu Lieux Popularité Position Noter ce lieu: Évaluation Classification Points de recyclage Étiquette Étiquettes Tourisme et Récréation Inutilisé Mettre à jour Restaurants végétariens et végétaliens Voir sur la carte WWOOF Pourquoi on l'aime? Travail adresse Catégorie Pays description email facebook 5 étoiles 2 étoiles localité prénom pas d'étoiles numéro de téléphone image code postal prix limace étiquettes twitter site Internet pourquoi nous aimons 