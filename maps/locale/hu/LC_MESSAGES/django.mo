��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �  "   �               %     :     S  
   a  	   l     v       	   �     �     �     �     �  
   �     �     �     �     �     �     �          .     @     L     S     _     f     |  
   �     �     �     �     �     �     �     �  
     
        &     7     <  )   Z     �     �     �     �  	   �     �     �     �     �     �     �          !     2  "   >     a     {     �     �     �  
   �     �     �     �     �     �     �     �     �     �                    %  	   )     3     <     D     M            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 2. kéz üzletek Új város hozzáadása Kép hozzáadása ehhez a helyhez! Add ezt a helyet! Lakcim alternatív kultúra Vissza %(city)s térkép Care & Health Kategória Gyermekek városok City Kávézó Kontakt Ország Hozzon Evés és ivás Eco cégek Szerkesztés Drága Divat Töltsük meg nekem! szűrők Élelmiszer üzletek és piacok Geometriátok GeoJSON otthoni ellátás Háztartás Olcsó Szélesség Levél Életmód és Család Hosszúság Mobilitás Mobilitás és turizmus Mérsékelt Legnépszerűbb (10+) civil szervezetek Natura 2000 Natura pályák Naturefriends házak Új város új helyen Nem elfogadható Más Parkok és természetvédelmi Gyalogos és kerékpáros infrastruktúra Kép Hely Helyek Népszerűség Pozíció Értékeld ezt a helyet: Értékelés Értékelések Recycling Points Címke Címkék Turizmus és kikapcsolódás Felhasználatlan Frissítés Vegetáriánus és vegán étterem Megtekintés a térképen WWOOF Miért szeretjük? Munka cím kategória ország leírás email Facebook teljes csillag fél csillag helység név nincs csillag telefonszám kép irányítószám ár URL-csiga címkék twitter weboldal Miért szeretjük 