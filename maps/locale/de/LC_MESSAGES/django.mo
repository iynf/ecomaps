��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �  $   �          +     3     F     e  	   y     �     �     �     �     �     �  	   �     �     �  
   �     �     �     �       !        0     G     U  	   ^     h     t     �     �  
   �     �     �     �     �     �     �     �  
             4  	   E     O  *   _     �     �     �     �     �     �     �     �     �  
   �     �               ,  !   :     \     s     y     �     �  	   �     �     �     �     �     �     �     �     �  
   �     �          	       	        &     2     :     C            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Second Hand Shops Füge eine neue Stadt hinzu Füge diesem Eintrag ein Foto hinzu! Füge diesen Ort hinzu! Adresse Alternative Kultur Zurück zur Karte von %(city)s Pflege & Gesundheit Kategorie Kinder Städte Stadt Café Kontakt Land Erstellen Essen und Trinken Öko-Unternehmen Bearbeiten Teuer Mode Fülle es für mich aus! Filter Lebensmittelgeschäften & Märkte Ortsangabe als GeoJSON Lieferserives Haushalt Preiswert Breitengrad Anschreiben Lebensstil & Familie Längengrad Mobilität Mobilität & Tourismus Mäßig Die Top (10 +) NROs Natura 2000 NaturaTrails Naturfreunde-Häuser Neue Stadt Füge einen neuen Ort hinzu Nicht zutreffend Sonstiges Parks und Natur Plätze für Fußgänger und Fahrradfahrer Bild Ort Orte Beliebtheit Position Bewerte diesen Ort: Bewerung Bewertungen Recycling-Einrichtungen Schlagwort Schlagworte Tourismus und Erholung Nicht verwendet Aktualisieren Vegetarische & vegane Restaurants Auf der Karte anzeigen WWOOF Warum liebn wir es? Arbeit Adresse Kategorie Land Beschreibung E-Mail Facebook ganzer Stern halber Stern Ort Name kein Stern Telefonnummer Bild Postleitzahl Preis Clean URL Schlagworte Twitter Webseite Warum wir es lieben 