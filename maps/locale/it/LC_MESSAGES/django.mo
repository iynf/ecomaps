��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �  %   �       	   .     8     L     j  	   x     �     �     �     �     �     �     �     �     �     �     �     �     �     �               6  
   H  	   S  
   ]     h     p     �  	   �     �     �     �     �     �     �     �     
          #     3     9  '   I     q     z     �     �  	   �     �     �     �     �     �     �     �                    1     B     H     Z  	   a  	   k     u     }     �     �     �     �  	   �     �     �     �     �     �     �     �     
                            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 negozi di seconda mano Aggiungi una nuova città Aggiungi un'immagine di questo luogo! Aggiungi questo posto! Indirizzo Cultura alternativa Torna alla mappa di %(city)s  Cura & Salute Categoria Bambini Città Città Caffetteria Contatto Nazione Crea Mangiare e Bere Aziende Eco Modifica Costoso Moda Compilalo per me! Filtri Alimentari & supermercati Geometria come GeoJSON Prodotti per casa Abitazione Economico Latitudine Lettera Stile di vita & Famiglia Longitudine Mobilità Mobilità & Turismo Moderato Più popolare (10+) ONG Natura 2000 Percorsi naturalistici Case Naturefriends Nuova città Nuovo luogo Non applicabile Altro Parchi e Natura Infastrutture pedonali e per biciclette Immagine Luogo Luoghi Popolarità Posizione Valuta questo luogo: Valutazione Valutazioni Punti di Riciclaggio Tagga Tags Turismo e Divertimento Disuso Aggiorna Ristoranti vegetariani e vegani Vedi sulla mappa WWOOF Perché ci piace? Lavoro indirizzo Categoria nazione descrizione email Facebook stella piena mezza stella località nome nessuna stella numero di telefono foto codice postale prezzo riferimenti tags twitter sito web perché ci piace 