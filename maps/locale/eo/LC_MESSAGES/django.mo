��    V      �     |      x     y     �     �     �     �     �     �     �                         !     -     5     =     D     X     f  	   k     u     }     �     �     �     �  	   �     �     �     �     �  	   	     	     	     *	     3	     F	     K	     W	     e	     z	  	   �	     �	     �	     �	  %   �	     �	     �	     �	  
   �	     �	     
     
     
     "
     3
     7
     <
     S
     Z
     a
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
                    #     )     .     3     ;     C  m  R     �     �     �     �               #     =  	   L     V     ^     d     i     p     y     �     �  
   �     �  
   �     �     �  	   �     �     �          !     '     5     >     E  	   Z  	   d     n     �     �     �     �     �     �  	   �     �     �     �     �  "        '     -     1  
   6     A     I     Z     `  
   g     r     z     �     �     �  "   �     �     �     �     �     �  	   �       	     	        "     .     :     F     K     P     \     j  	   p     z     �     �     �     �     �            	       '   K   I           T             U                        Q      2   C       ?   /             %   G                             8              (   7       P   A      5      @       E           )              .   D   =   3   !   
      4              <                        F   &   S   "   1                      ;          >          6   $   N   L          +   #                 R   :           B   *   0       9   O                 V   H   ,   -   M   J    2nd hand shops Add a new city Add a picture to this place! Add this place! Address Alternative culture Back to %(city)s map Care & Health Category Children Cities City Coffee Shop Contact Country Create Eating and Drinking Eco companies Edit Expensive Fashion Fill it for me! Filters Food stores & markets Geometry as GeoJSON Home Supply Household Inexpensive Latitude Letter Lifestyle & Family Longitude Mobility Mobility & Tourism Moderate Most popular (10+) NGOs Natura 2000 Natura trails Naturefriends Houses New city New place Non Applicable Other Parks and Nature Pedestrian and bicycle infrastructure Picture Place Places Popularity Position Rate this place: Rating Ratings Recycling Points Tag Tags Tourism and Recreation Unused Update Vegetarian & vegan restaurants View on map WWOOF Why we love it? Work address category country description email facebook full star half star locality name no star phone number picture postcode price slug tags twitter website why we love it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 10:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Brokanto Aldoni novan urbon Aldonu bildon al tiu ejo! Aldoni tiun ejon! Adreso Alternativa kulturo Reiri al la %(city)s mapo Sano kaj zorgo Kategorio Infanoj Urboj Urbo Kafejo Kontakto Landoj Krei Manĝo kaj trinkado Ekofirmaoj Redakti Multekosta Modo Plenigu anstataŭ mi! Filtriloj Manĝvendejoj kaj merkatoj Geometrio kiej GeoJSON Aĵoj por la hejmo Hejmo Malmultekosta Latitudo Litero Vivstilo kaj familio Longitudo Movebleco Movebleco kaj turismo Modera Plej popularaj NRO Naturo 2000 Natura-spuroj Naturefriends domoj Nova urbo Nova ejo Ne aplikebla Aliaj Parkoj kaj naturo Pasanta kaj bicikla infrastrukturo Bildo Ejo Ejoj Populareco Pozicio Taksu tiun ejon: Takso Taksoj Reciklejoj Etikedo Etikedoj Turismo kaj amiziĝoj Neuzata Ŝanĝi Vegetarianaj & veganaj restoracioj Vidi sur mapo WWOOF Kial ni ŝatas? Laboro adreso categorio lando priskribo retadreso vizaĝlibro plena stelo duona stelo loko nomo neniu stelo telefonnumero bildo poŝtkodo prezo ĵetonvorto etikedoj pepejo retejo kial ni ŝatas 