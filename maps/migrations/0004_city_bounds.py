# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2015-11-11 15:22
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0003_auto_20151027_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='bounds',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=dict),
        ),
    ]
