from collections import namedtuple

from django.utils.translation import ugettext_lazy as _
from django.contrib.staticfiles.templatetags.staticfiles import static


Category = namedtuple('Category', ['slug', 'name', 'icon', 'color', 'subcategories'])
Subcategory = namedtuple('Subcategory', ['category', 'slug', 'name'])


food = Category('food', _("Eating and Drinking"), static('img/categories/food.png'), 'green', (
    Subcategory('food', 'coffee_shop', _("Coffee Shop")),
    Subcategory('food', 'vegetarian_restaurant', _("Vegetarian & vegan restaurants")),
    Subcategory('food', 'food_stores_markets', _("Food stores & markets")),
))


home = Category('home', _("Home Supply"), static('img/categories/home.png'), 'brown', (
    Subcategory('home', 'household', _("Household")),
))


lifestyle = Category('lifestyle', _("Lifestyle & Family"), static('img/categories/lifestyle.png'), 'magenta', (
    Subcategory('lifestyle', 'parks_nature', _("Parks and Nature")),
    Subcategory('lifestyle', 'children', _("Children")),
    Subcategory('lifestyle', 'fashion', _("Fashion")),
    Subcategory('lifestyle', 'care_health', _("Care & Health")),
    Subcategory('lifestyle', 'natura_2000', _("Natura 2000")),
    Subcategory('lifestyle', 'second_hand_shops', _("2nd hand shops")),
))


mob_tourism = Category('mob_tourism', _("Mobility & Tourism"), static('img/categories/mobility.png'), 'blue', (
    Subcategory('mob_tourism', 'mobility', _("Mobility")),
    Subcategory('mob_tourism', 'tourism_recreation', _("Tourism and Recreation")),
    Subcategory('mob_tourism', 'pedestrian_bicycle_infra', _("Pedestrian and bicycle infrastructure")),
    Subcategory('mob_tourism', 'naturefriends_houses', _("Naturefriends Houses")),
    Subcategory('mob_tourism', 'natura_trails', _("Natura trails")),
    Subcategory('mob_tourism', 'wwoof', _("WWOOF")),
))


work = Category('work', _("Work"), static('img/categories/work.png'), 'yellow', (
    Subcategory('work', 'alternative_culture', _("Alternative culture")),
    Subcategory('work', 'ngo', _("NGOs")),
    Subcategory('work', 'eco_companies', _("Eco companies")),
))


other = Category('other', _("Other"), static('img/categories/other.png'), 'dark', (
    Subcategory('other', 'recycling_points', _("Recycling Points")),
))


CATEGORIES = (food, home, lifestyle, mob_tourism, work, other)
EMPTY_CATEGORY = ('', _("Category")),


def get_category_choices():
    choices = []
    for cat in CATEGORIES:
        subcats = tuple((subcat.slug, subcat.name) for subcat in cat.subcategories)
        choices.append(
            (cat.name, subcats)
        )
    return EMPTY_CATEGORY + tuple(choices)


CATEGORY_CHOICES = get_category_choices()


def get_cat(subcat):
    for CAT in CATEGORIES:
        for SUBCAT in CAT.subcategories:
            if SUBCAT.slug == subcat:
                return CAT
    return ''


NA, INEXPENSIVE, MODERATE, EXPENSIVE = 0, 1, 2, 3
PRICE_CHOICES = (
    (NA, _("Non Applicable")),
    (INEXPENSIVE, _("Inexpensive")),
    (MODERATE, _("Moderate")),
    (EXPENSIVE, _("Expensive")),
)
