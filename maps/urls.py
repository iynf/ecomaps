from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import (
    Map, New, Detail, Update,
    CityNew, CityUpdate, CityList,
    RatingView, RatingDetailView,
    BboxView, CitiesAutocompleteView, GeoJsonListView,
)

urlpatterns = [
    url(r'^place/new/$', login_required(New.as_view()), name='new'),
    url(r'^place/(?P<pk>\d+)/(?P<slug>[\w-]+)/$', Detail.as_view(), name='detail'),
    url(r'^place/(?P<pk>\d+)/(?P<slug>[\w-]+)/edit/$', login_required(Update.as_view()), name='update'),
    url(r'^rating/$', RatingView.as_view(), name='rating'),
    url(r'^rating/(?P<rating>\d\.\d)/$', RatingDetailView.as_view(), name='rating_detail'),
    url(r'^city/$', login_required(CityList.as_view()), name='cities'),
    url(r'^city/new/$', login_required(CityNew.as_view()), name='city_new'),
    url(r'^city/(?P<pk>[a-z-]+)/$', CityUpdate.as_view(), name='city'),
    url(r'^map/(?:(?P<city>[\w-]+)/)?$', Map.as_view(), name='map'),
    url(r'^api/geojson/(?:(?P<pk>\d+)/)?(?:(?P<city>[\w-]+)/)?$', GeoJsonListView.as_view(), name='geojson'),
    url(r'^api/bbox/(?:(?P<city>[\w-]+)/)?$', BboxView.as_view(), name='bbox'),
    url(r'^api/cities/$', CitiesAutocompleteView.as_view(), name='city_autocomplete')
]
