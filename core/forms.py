import logging

from django import forms
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.core.urlresolvers import resolve
from django.db.transaction import atomic
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.shortcuts import get_current_site

from django_comments.models import Comment
from registration.forms import RegistrationForm
from maps.models import City
from maps.choices import CATEGORIES
from .models import Profile
from .utils import get_ip

logging.basicConfig(format='%(asctime)s %(message)s')
User = get_user_model()


class HoneyRegistrationForm(RegistrationForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'autocomplete':'off'}),
                           required=False)

    def clean_name(self):
        """Check the honeypot"""
        data = self.cleaned_data
        flies, username, email = data['name'], data['username'], data['email']
        if flies:
            logging.warning("SPAM", flies, username, email)
            raise forms.ValidationError("You're a spammer, I guess...")
        return flies


class CommentForm(forms.ModelForm):
    object_url = forms.CharField()
    name = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Comment
        fields = ['comment', 'object_url']

    def clean_object_url(self):
        data = self.cleaned_data['object_url']
        view, args, kwargs = resolve(data)
        self.content_object = get_object_or_404(view.view_class.model, **kwargs)
        return data

    def clean_name(self):
        """Check the honeypot"""
        flies = self.cleaned_data['name']
        if flies:
            print('SPAM: ', flies, self.cleaned_data['comment'])
            raise forms.ValidationError("You're a spammer")
        return flies

    def save(self):
        comment = self.instance
        comment.site = get_current_site(self.request)
        comment.content_object = self.content_object
        comment.user = self.request.user
        comment.submit_date = timezone.now()
        comment.ip_address = get_ip(self.request)
        comment.save()
        return comment


class HomeForm(forms.Form):
    q = forms.CharField(label=_("What are you looking for?"))
    city = forms.CharField(label=_("Choose your city…"), widget=forms.HiddenInput)
    cat = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=((cat.slug, cat.name) for cat in CATEGORIES))


class ProfileForm(forms.ModelForm):
    username = forms.CharField(label=_("username"), required=True)
    email = forms.EmailField(label=_("email"), required=True)
    first_name = forms.CharField(label=_("first name"), required=False, max_length=30)
    last_name = forms.CharField(label=_("last name"), required=False, max_length=30)

    user_fields = ['username', 'email', 'first_name', 'last_name']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        for field_name in self.user_fields:
            kwargs['initial'][field_name] = getattr(self.user, field_name)
        super().__init__(*args, **kwargs)
        self.fields['city'].widget.choices = City.objects.choices(_("City"))
        for name, field in self.fields.items():
            field.widget.attrs['class'] = 'validate'
            if field.widget.__class__.__name__ == 'Textarea':
                field.widget.attrs['class'] += ' materialize-textarea'
        for name, errors in self.errors.items():
            self.fields[name].widget.attrs['class'] += ' invalid card-panel'

    class Meta:
        model = Profile
        fields = [
            'username', 'email', 'first_name', 'last_name',
            'country', 'city', 'avatar',
            'description', 'why_i_joined',
        ]

    def save_user(self):
        for field_name in self.user_fields:
            setattr(self.user, field_name, self.cleaned_data[field_name])
            self.user.save()
        return self.user

    @atomic
    def save(self, commit=True):
        profile = self.instance
        if not hasattr(self.instance, 'user'):
            profile.user = self.user
            self.save(commit=False)
        self.save_user()
        if commit:
            profile.save()
        return profile
