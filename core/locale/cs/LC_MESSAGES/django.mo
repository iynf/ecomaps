��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	  #   �	     
      
  i   1
     �
  	   �
  8   �
     �
     �
               '     :  %   W     }  
   �  :   �     �     �     �     	               )     ;     A     O  	   e     o  !   �     �     �     �     �     �     �  *   �          *  #   7     [     b     i     p     �  
   �  	   �     �     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 o /kritéria/ /zúčastnit se/ O Přidej nové místo Přidej si fotku ke svému profilu! Zpět na úvodní stránku Buď součástí Chovat se trvale udržitelně v každodenním životě <br><strong></strong>je snažší než si myslíš Vyber své město… Kritéria Objev nejvíce trvale udržitelná místa v tvém okolí Hledej Vyjádři se! Zúčastnit se Ahoj %(name)s ! Domovská stránka Asi bys to neměl vyplňovat Přidal/a jsem se k ECOmaps protože: Důležitá data Přidej se Přidej se a přidávej další trvale udržitelná místa Poslední příspěvky Přihlášení Přihlášení Odhlásit se Mapa Mapa %(city)s Člen od %(date)s Více Nová zpráva Zatím žádný popis Povolení Osobní informace Zásady ochrany osobních údajů Profil Úprava profilu Profily Přihlásit se Odhlášení Přihlášení Omlouváme se, žádné místo nenalezeno. Podmínky služby Co hledáte? Komentovat mohou jen přihlášení o mně avatar město Křestní jméno z Belgie z Německa ze Srbska z Nizozemí Příjmení Uživatelské jméno 