��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	  	   �	     �	     �	     �	  $   �	     
  	   
  i   (
     �
     �
  1   �
     �
  
   �
     �
                     3     T  	   d  )   n     �     �     �     �     �     �     �     �  
   �     �          (     ?     [     c     x  
   �     �     �  /   �     �     �                /     6     =  
   B     M     \     i     u     }     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /circa/ /criteri/ /contribuisci/ Circa Aggiungi un nuovo posto Aggiungi un'immagine al tuo profilo! Torna indietro Partecipa Essere sostenibili ogni giorno della propria vita <br><strong>è più facile di quello che pensi</strong> Scegli la tua città… Criteri Scopri i posti più sostenibili nei tuoi dintorni Esplora Esprimiti! Contribuisci Ciao %(name)s! Casa Non compilare questa parte Sono entrato in ECOmaps perché: Date importanti Partecipa Contribuisci e aggiungi posti sostenibili Ultimi aggiornamenti Accesso Accedi Uscire Mappa Mappa di %(city)s Membro da %(date)s Altro Nuovo Post Descrizione ancora non presente Permessi Informazioni personali Politica sulla riservatezza Profilo Modifica del profilo Profili Registrati Registrazione Registrazione Siamo spiacenti, nessun posto é stato trovato. Termini del servizio Cosa stai cercando? Devi accedere per commentare a proposito di me avatar città nome dal Belgio dalla Germania dalla Serbia dall'Olanda cognome nome utente 