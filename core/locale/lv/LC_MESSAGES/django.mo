��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	     �	     
     ,
  P   >
     �
  
   �
  5   �
  	   �
     �
  
                *   .  #   Y     }     �  2   �     �     �     �     �                    1     9     F  	   S     ]     t     �     �     �     �     �     �  '   �            1   .     `     i     q     z     �     �     �     �     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 / Par mums / / kritēriji / /iesaistīties/ Par Pievienot jaunu vietu Pievienojiet profila attēlu! Atgriezties galvenajā lapā Esi daļa no šī Būt videi draudzīgam ikdienā <br><strong>ir vieglāk, nekā tu domā</strong> Izvēlies savu pilsētu ... kritēriji Atklāj videi draudzīgākās vietas Tavā apkārtnē Izpētīt Izpaud sevi! Iesaisties Sveiki, %(name)s! Galvenā lapa Es domāju, ka šo nevajadzētu aizpildīt Es pievienojos ECOmaps, tāpēc ka: Svarīgi datumi Pievienoties Pievienojies un pievieno videi draudzīgās vietas Jaunākie ieraksti Pieslēgties Pieslēgties Izrakstīties karte  %(city)s karte Biedrs no %(date)s Vairāk Jauns raksts Nav apraksta Atļaujas Privātā informācija Privātuma politika Profils Profila rediģēšana Profili Pieraksīties Pierakstīties Pierakstīties Atvainojiet, šī vieta netika atrasta. Lietošanas noteikumi Ko Tu vēlies atrast? Jums nepieciešams pierakstīties, lai komentētu Par mani profils pilsēta vārds no Beļģijas no Vācijas no Serbijas no Nīderlandes uzvārds lietotājvārds 