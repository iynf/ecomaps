��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =  
   �	     �	     �	     �	     �	     �	     
  
    
  S   +
     
  	   �
  /   �
     �
     �
  
   �
     �
  	   �
     	     %     A     N  $   U     z     �     �     �     �     �     �     �     �     �     �       	        "     *     :     C     O     [     g     �     �  &   �     �     �     �     �  	   �     �  	               	   *     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /eo/about/ /eo/criteria/ /eo/get-involved/ Pri ni Aldonu novan ejon Aldonu bildon al via profilo! Reiri al ĉefpaĝo Partopreno Esti daŭripova en ĉiutaga vivo <br><strong>estas pli facila ol vi pensas</strong> Elektu vian urbon… Kriterioj Malkovru la plej daŭripovajn ejojn ĉirkaŭ vi Esploru Esprimiĝu! Envolviĝu Saluton %(name)s! Ĉefpaĝo Prefere ne plenigu ĉi-tion Mi aliĝis al ECOmaps ĉar: Gravaj datoj Aliĝu Aliĝo kaj aldonu daŭripovajn ejojn Lastaj redaktoj Ensaluti Ensaluti Elsaluti Mapo Mapo de %(city)s Membro ekde %(date)s Pli Nova redaktaĵo Neniu priskribo ankoraŭ Permesoj Personaj informoj Privateco Profilo Profila redakto Profiloj Registriĝu Registriĝi Registriĝi Bedaŭrinde neniu ejo trovita. Uzkondiĉoj Kion vi serĉas? Vi bezonas esti ensalutita por komenti pri mi profilbildo urbo persona nomo el Belgio el Germanio el Serbio el Nederlando familia nomo uzantnomo 