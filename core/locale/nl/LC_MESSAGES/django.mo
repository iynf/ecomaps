��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	  !   �	     
     !
  V   )
     �
     �
  /   �
     �
     �
     �
     �
     	  %        4     S  	   d  %   n     �     �     �  	   �     �     �     �     �     �     �               6     D     L  	   ]  	   g  	   q  	   {     �  
   �     �  ,   �     �     �          	               ,     8  
   F     Q     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /over/ / Criteria / /betrokken raken/ Over Voeg een nieuwe plek toe Voeg een foto aan je profiel toe! Terug naar huis Doe mee Duurzaam zijn in het dagelijkse leven <br><strong>is makkelijker dan je denkt</strong> Kies je plaats ... criteria Ontdek de meest duurzame plekken in je omgeving onderzoeken Druk jezelf uit! Betrokken raken Hello %(name)s! Huis Ik denk dat je dit niet moet invullen Ik werd lid van ECOmaps omdat: Belangrijke data Toetreden Doe mee en voeg duurzame plaatsen toe Recente ingaven Log in Log in Uitloggen Kaart Kaart van %(city)s Lid sinds %(date)s Meer Nieuw bericht nog geen beschrijving Machtigingen Persoonlijke informatie Privacybeleid Profiel profiel bewerken profielen Aanmelden Aanmelden Aanmelden Sorry, geen plaats gevonden. Diensttijd Waar bent u naar op zoek? Je moet ingelogd zijn om commentaar te geven over mij avatar stad Voornaam uit België uit Duitsland uit Servië uit Nederland achternaam gebruikersnaam 