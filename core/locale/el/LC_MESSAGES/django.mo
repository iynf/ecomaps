��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	  
   �	     �	     �	  7   �	  H   
  +   b
  =   �
  �   �
  (   ~     �  R   �                2     M     g  F   u  0   �  +   �       @   -  -   n     �     �     �     �  #   �     �          *  4   G     |  +   �  M   �       #        =     J     Y     h  $   w     �     �  D   �          *     1  
   :     E     Y     q     �     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /about/ /criteria/ /get-involved/ Σχετικά με Πρόσθεσε ένα καινούργιο μέρος Πρόσθεσε μια φωτογραφία στο προφίλ σου! Πίσω στην Αρχική Σελίδα Πάρε μέρος σε αυτή την προσπάθεια Το να ζεις με βιώσιμο τρόπο στην καθημερινότητά σου <br><strong>είναι ευκολότερο απ' όσο νομίζεις</strong> Επέλεξε την πόλη σου… Κριτήρια Ανακάλυψε τα πιο sustainable μέρη στην περιοχή σου Εξερεύνησε Εκφράσου! Δράσε μαζί μας Γεια σου %(name)s! Αρχική  Μάλλον δε θα έπρεπε να το συμπληρώσεις Έγινα μέλος στο ECOmaps διότι: Σημαντικές ημερομηνίες Γίνε μέλος Πάρε μέρος και πρόσθεσε sustainable μέρη Τελευταίες Καταχωρήσεις Σύνδεση Σύνδεση Έξοδος Χάρτης Χάρτης του/της %(city)s Μέλος από %(date)s Περισσότερα Νέα Kοινοποίηση Δεν υπάρχει περιγραφή ακόμη  Δικαιώματα Προσωπικές πληροφορίες Πολιτική Προστασίας Προσωπικών Δεδομένων Προφίλ  Επεξεργασία προφίλ Προφίλ Εγγραφή Εγγραφή Εγγραφή Το μέρος δε βρέθηκε. Όροι χρήσης Τι ψάχνεις; Απαιτείται σύνδεση για να σχολιάσεις σχετικά μ' εμένα avatar πόλη όνομα από Βέλγιο από Γερμανία από Σερβία από Ολλανδία επίθετο ψευδώνυμο 