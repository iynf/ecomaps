��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	  "   �	     
     /
  Z   F
     �
  	   �
  ;   �
                    *     <  (   D     m     �     �  /   �     �     �     �                    +     B     G     \     w     �     �     �     �     �  
   �  
   �  
   �            !     >  (   Q     z     �     �     �     �     �     �     �     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /à propos/ /Critères/ /être impliqué/ A propos Ajouter un nouvel endroit Ajouter une photo à votre profil! Retour à l'accueil Rejoignez le mouvement Être durable dans la vie quotidienne <br><strong>est plus facile que vous pensez</strong> Choisissez votre ville ... Critères Découvrez les endroits les plus durables dans les environs Explorer Exprimez-vous! Être impliqué Bonjour %(name)s! Accueil Je suppose que tu ne devrais pas remplir J'ai rejoint ECOmaps parce que: Dates importants Joindre Inscrivez-vous et ajouter des endroits durables dernières entrées S'identifier S'identifier Se déconnecter Carte Plan de %(city)s Membre depuis %(date)s Plus Nouvelle publication Pas encore de description  Autorisations Informations personnelles Politique de confidentialité Profil Édition du profil Profils S'inscrire S'inscrire S'inscrire Désolé, aucun endroit trouvé. Termes du contrat de service Que cherchez-vous? Vous devez vous connecter pour commenter À propos de moi Avatar ville Prénom de la Belgique de l'Allemagne de la Serbie des Pays-Bas Nom  Nom d'utilisateur 