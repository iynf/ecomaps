��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	  
   �	  	   �	     �	  #   �	     
  	   +
  J   5
     �
  	   �
  0   �
     �
     �
     �
     �
  
      +        7     S     d  (   m     �     �     �     �     �     �     �     �     �               -     H     ^     e     t     |     �     �  +   �     �     �  4   �  
   .     9     @     F     N     Z     j     v     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /ueber/ /Kriterien/ /mach-mit/ Über uns Füge einen neuen Ort hinzu Füge deinem Profil ein Foto hinzu! Zurück zur Startseite Sei dabei Im Alltag nachhaltig sein <br><strong>ist einfacher als du denkst</strong> Wähle deine Stadt… Kriterien Finde die nachhaltigsten Orte in deiner Umgebung Erkunde Lass es raus! Mach mit Hallo %(name)s! Startseite Ich denke, du solltest das nicht ausfüllen Ich mache bei ECOmaps weil: Wichtige Termine Mach mit Mach mit und füge nachhaltige Ort hinzu Neueste Einträge Anmelden Anmelden Abmelden Karte Karte von %(city)s Mitglied seit %(date)s Mehr Neuer Artikel Noch keine Beschreibung Berechtigungen Persönliche Informationen Datenschutzrichtlinie Profil Profil ändern Profile Anmelden Registriere dich Registriere dich Entschuldigung, es wurde kein Ort gefunden. Nutzungsbedingungen Wonach suchts du? Du musst dich anmelden, um einen Kommentar abzugeben Über mich Avatar Stadt Vorname aus Belgien aus Deutschland aus Serbien aus den Niederlanden Nachname Benutzername 