��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	  &   �	     
  	   +
  S   5
     �
  
   �
  3   �
     �
     �
  
   �
                      A     ]     o  (   v     �     �     �     �     �     �     �     �     �               "     7     P     W     e     l     x     �  "   �     �      �      �  	                        %     1  
   =     H  	   [     e     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /sobre/ /critérios/ /envolve-te/ Sobre Adiciona um novo lugar Adiciona uma fotografia ao teu perfil! Voltar à página inicial Participa Ser sustentável no teu dia-a-dia <br><strong>é mais fácil do que pensas</strong> Escolhe a tua cidade… Critérios Descobre os lugares mais sustentáveis à tua volta Explorar Expressa-te! Envolve-te Olá %(name)s! Página inicial Talvez não devas preencher isto Eu aderi ao ECOmaps porque: Datas importantes Aderir Junta-te e adiciona locais sustentáveis Últimas Entradas Entrar Entrar Sair Mapa Mapa de %(city)s Membro desde%(date)s Mais Nova Publicação Sem descrição Permissões Informação pessoal Política de privacidade Perfil Editar perfil Perfis Criar conta Criar conta Criar conta Desculpe, nenhum local encontrado. Termos de serviço Do que é que estás à procura? Precisas de entrar para comentar sobre mim avatar cidade nome da Bélgica da Alemanha da Sérvia dos Países Baixos sobrenome nome de usuário 