��    8      �  O   �      �     �  
   �     �     �               0     =  P   K     �     �  9   �     �     �               *  "   /     R     l     |     �     �     �     �     �     �     �     �     �     �     �               )     8     @     P     Y     a     i     q     �     �     �     �     �     �  
   �     �     �     	       	   *     4  m  =     �	     �	     �	     �	     �	  !   �	     
     /
  X   =
     �
     �
  :   �
  
   �
                     /     6     P     l     |  ;   �     �     �     �     �           	          )     /     B     V     b     t     �     �     �     �     �     �      �     �       (        :     A     H     O     [     c     u  	   {     �     �     "                )      $         %          7             3      (          	   1   2   '   ,         5                 /                  &   0   +                   .         !                  6   8   *           -      
                      4                                #    /about/ /criteria/ /get-involved/ About Add a new place Add a picture to your profile! Back to Home Be part of it Being sustainable in everyday life <br><strong>is easier than you think</strong> Choose your city… Criteria Discover the most sustainable places in your surroundings Explore Express yourself! Get involved Hello %(name)s! Home I guess thou shouldn’t fill this I joined ECOmaps because: Important dates Join Join and add sustainable places Last Entries Log In Log in Log out Map Map of %(city)s Member since %(date)s More New Post No description yet Permissions Personal info Privacy policy Profile Profile editing Profiles Sign In Sign Up Sign up Sorry, no place found. Term of service What are you looking for? You need to login to comment about me avatar city first name from Belgium from Germany from Serbia from the Netherlands last name username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-18 09:37+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 /ról ről/ /kritériumok/ /részt venni/ Körülbelül Új hely hozzáadása Kép hozzáadása a profiljához! Vissza a főoldalra Legyen része Mivel a fenntartható mindennapi életben <br><strong>könnyebb, mint gondolná</strong> Válasszon várost ... Kritériumok Fedezze fel a legnagyobb fenntartható hely a környéken, Fedezd fel Fejezd ki magad! Részt venni Helló %(name)s! Otthon Gondolom te nem írja ezt Csatlakoztam ECOmaps, mert: Fontos dátumok Csatlakozik Csatlakozz, és adjunk hozzá a fenntartható települések utolsó bejegyzések Belépek Bejelentkezés Kijelentkezés Térkép Térképe %(city)s Tag %(date)s Több új hozzászólás Nincs leírás még Engedélyek Személyes adatok Adatvédelem Profil Profil szerkesztése profilok Bejelentkezés Regisztrálj Regisztrálj Sajnáljuk, nincs helye talált. Term szolgáltatás Mit keresel? Be kell jelentkezned a hozzászóláshoz rólam avatar város keresztnév Belgium Németországból Szerb Hollandia vezetéknév felhasználónév 