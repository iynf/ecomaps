# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-18 09:37+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Permissions"
msgstr "Engedélyek"

msgid "Important dates"
msgstr "Fontos dátumok"

msgid "Personal info"
msgstr "Személyes adatok"

msgid "Activate selected users"
msgstr ""

msgid "Inactivate selected users"
msgstr ""

msgid "What are you looking for?"
msgstr "Mit keresel?"

msgid "Choose your city…"
msgstr "Válasszon várost ..."

msgid "username"
msgstr "felhasználónév"

msgid "email"
msgstr ""

msgid "first name"
msgstr "keresztnév"

msgid "last name"
msgstr "vezetéknév"

msgid "City"
msgstr ""

msgid "country"
msgstr ""

msgid "Country"
msgstr ""

msgid "city"
msgstr "város"

msgid "avatar"
msgstr "avatar"

msgid "about me"
msgstr "rólam"

msgid "I joined ECOmaps because:"
msgstr "Csatlakoztam ECOmaps, mert:"

msgid "Profile"
msgstr "Profil"

msgid "Profiles"
msgstr "profilok"

msgid "Log In"
msgstr "Belépek"

msgid "Sign Up"
msgstr "Regisztrálj"

msgid "I guess thou shouldn’t fill this"
msgstr "Gondolom te nem írja ezt"

msgid "Express yourself!"
msgstr "Fejezd ki magad!"

msgid "You need to login to comment"
msgstr "Be kell jelentkezned a hozzászóláshoz"

msgid "Log in"
msgstr "Bejelentkezés"

msgid "Add a picture to your profile!"
msgstr "Kép hozzáadása a profiljához!"

msgid "No description yet"
msgstr "Nincs leírás még"

#, python-format
msgid "Member since %(date)s"
msgstr "Tag %(date)s"

msgid "Edit"
msgstr ""

msgid "Last Entries"
msgstr "utolsó bejegyzések"

#, fuzzy
#| msgid "Add a picture to your profile!"
msgid "Add a picture to this place!"
msgstr "Kép hozzáadása a profiljához!"

msgid "Profile editing"
msgstr "Profil szerkesztése"

msgid "Update"
msgstr ""

msgid "/about/"
msgstr "/ról ről/"

msgid "About"
msgstr "Körülbelül"

msgid "Term of service"
msgstr "Term szolgáltatás"

msgid "Privacy policy"
msgstr "Adatvédelem"

msgid "Back to Home"
msgstr "Vissza a főoldalra"

#, python-format
msgid "Hello %(name)s!"
msgstr "Helló %(name)s!"

msgid "Home"
msgstr "Otthon"

msgid "Sign up"
msgstr "Regisztrálj"

#. Translators: Don't translate <br> nor <strong>.
msgid ""
"Being sustainable in everyday life <br><strong>is easier than you think</"
"strong>"
msgstr ""
"Mivel a fenntartható mindennapi életben <br><strong>könnyebb, mint gondolná</"
"strong>"

msgid "Discover the most sustainable places in your surroundings"
msgstr "Fedezze fel a legnagyobb fenntartható hely a környéken,"

msgid "Explore"
msgstr "Fedezd fel"

msgid "New Post"
msgstr "új hozzászólás"

msgid "Topics"
msgstr ""

msgid "More"
msgstr "Több"

msgid "Be part of it"
msgstr "Legyen része"

msgid "Join and add sustainable places"
msgstr "Csatlakozz, és adjunk hozzá a fenntartható települések"

msgid "from Germany"
msgstr "Németországból"

msgid "from Belgium"
msgstr "Belgium"

msgid "from Serbia"
msgstr "Szerb"

msgid "from the Netherlands"
msgstr "Hollandia"

msgid "Join"
msgstr "Csatlakozik"

#, python-format
msgid "Map of %(city)s"
msgstr "Térképe %(city)s"

msgid "Map"
msgstr "Térkép"

msgid "Sorry, no place found."
msgstr "Sajnáljuk, nincs helye talált."

msgid "Sign In"
msgstr "Bejelentkezés"

msgid "/get-involved/"
msgstr "/részt venni/"

msgid "Get involved"
msgstr "Részt venni"

msgid "Add a new place"
msgstr "Új hely hozzáadása"

msgid "/criteria/"
msgstr "/kritériumok/"

msgid "Criteria"
msgstr "Kritériumok"

msgid "Cities"
msgstr ""

msgid "Log out"
msgstr "Kijelentkezés"

#~ msgid "I joind ECOmaps because:"
#~ msgstr "Azt joind ECOmaps, mert:"

#~ msgid "/terms/"
#~ msgstr "/ Feltételek /"
