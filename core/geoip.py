import warnings

from geoip2.database import Reader
from geoip2.errors import AddressNotFoundError

from django.conf import settings


_country_cache = dict()


def get_country(remote_ip):
    if not remote_ip:
        return ''

    if remote_ip in _country_cache:
        return _country_cache[remote_ip]

    try:
        country_db = Reader(settings.MAXMIND_COUNTRY_PATH).country
    except FileNotFoundError:
        warnings.warn("""
            MaxMind database not found here: """ + settings.MAXMIND_COUNTRY_PATH,
            ResourceWarning,
            stacklevel=2)
        return ''

    try:
        country = country_db(remote_ip)
    except AddressNotFoundError:
        warnings.warn("""
            IP was not found in the MaxMind database""",
            ResourceWarning,
            stacklevel=2)
        return ''

    iso_code = country.country.iso_code
    if iso_code:
        _country_cache[remote_ip] = iso_code
    return iso_code
