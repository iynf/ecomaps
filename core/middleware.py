from .utils import get_ip, get_country


class VisitorIpMiddleware(object):
    def process_request(self, request):
        request.ip = get_ip(request)


class GeoipMiddleware(object):
    def process_request(self, request):
        request.country = get_country(request)
