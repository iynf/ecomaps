from django.conf import settings


def staging(request):
    return {'staging': settings.STAGING}
