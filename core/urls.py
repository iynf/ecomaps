from django.conf.urls import url, include
from registration.backends.simple.views import RegistrationView

from .views import HomeView, UserView, ProfileView, ProfileUpdateView, CommentView, CommentDetailView
from .forms import HoneyRegistrationForm

urlpatterns = [
    url(r'^user/$', UserView.as_view(), name='user'),
    url(r'^user/(?P<username>[\w-]+)/$', ProfileView.as_view(), name='profile'),
    url(r'^user/(?P<username>[\w-]+)/edit/$', ProfileUpdateView.as_view(), name='profile_update'),
    url(r'^comment/$', CommentView.as_view(), name='comment'),
    url(r'^comment/(?P<pk>\d+)/$', CommentDetailView.as_view(), name='comment_detail'),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^search/$', HomeView.as_view(), name='search'),
    # Taken from registration.backends.simple.urls:
    url(r'^register/$',
        RegistrationView.as_view(form_class=HoneyRegistrationForm),
        name='registration_register'),
    url(r'^', include('registration.auth_urls')),
]
