from ipaddress import ip_address

from . import geoip


def get_ip(request):
    """Return the first valid IP from headers."""
    raw_ips = request.META.get('HTTP_X_FORWARDED_FOR', '').split(",")
    for raw_ip in raw_ips:
        try:
            return ip_address(raw_ip.strip()).exploded
        except ValueError:
            continue


def get_country(request):
    if not request.session.get('country'):
        request.session['country'] = geoip.get_country(request.ip)
        request.session.save()
    return request.session['country']


def qstr(GET, mark='?'):
    """
    Turn the request.GET dict into an URL query string
    """
    groups = [[(k, param) for param in lst] for k, lst in GET.items()]
    flat = [item for sub in groups for item in sub]
    query_string = '&'.join(k + '=' + v for k, v in flat if v) if flat else ''
    return mark + query_string if query_string else ''


def round_half(n):
    return 0. if n is None else round(n * 2) / 2


def star_to_list(rating):
    if rating.is_integer():
        stars = int(rating)
        return ['full'] * stars + ['empty'] * (5 - stars)
    else:
        stars = int(str(rating)[0])
        return ['full'] * stars + ['half'] + ['empty'] * (4 - stars)
