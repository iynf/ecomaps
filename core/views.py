from collections import namedtuple
from random import choice
from os import path

from django.views import generic
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.staticfiles.templatetags.staticfiles import static

from django_comments.models import Comment

from maps.choices import CATEGORIES
from educational.models import Post, Topic
from .mixins import JsonResponseMixin
from .models import Profile
from .forms import HomeForm, ProfileForm, CommentForm


class CommentView(JsonResponseMixin, generic.CreateView):
    model = Comment
    form_class = CommentForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response(self.object)

    def form_invalid(self, form):
        return self.render_to_response(None)

    def get_data(self, comment):
        if comment:
            return {'url': reverse('comment_detail', kwargs={'pk': comment.pk})}
        return {'error': 'Invalid form'}


class CommentDetailView(generic.DetailView):
    model = Comment
    template_name = 'comment.html'


class HomeView(generic.FormView):
    form_class = HomeForm
    template_name = 'home.html'

    def background(self):
        bg_dir = settings.BACKGROUNDS_DIR
        backgrounds = [static(path.join(bg_dir, name)) for name in settings.BACKGROUNDS]
        return choice(backgrounds)

    def categories(self):
        return CATEGORIES

    def post_types(self):
        return Post.POST_TYPES_PLURAL

    def topics(self):
        return Topic.objects.all()


class UserView(generic.RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            return reverse('profile', kwargs={'username': self.request.user})
        raise Http404


class ProfileView(generic.DetailView):
    model = Profile
    context_object_name = 'profile'
    template_name = 'core/profile_detail.html'

    def get_object(self):
        user = get_object_or_404(
            get_user_model(),
            username=self.kwargs['username'])
        DummyProfile = namedtuple('Profile', ['user', 'dummy'])
        dummy_profile = DummyProfile(user, True)
        return getattr(user, 'profile', dummy_profile)

    def places(self):
        return self.object.user.places.order_by('-id')[:4]


class ProfileUpdateView(UserPassesTestMixin, generic.UpdateView):
    model = Profile
    form_class = ProfileForm

    def test_func(self):
        return self.request.user.username == self.kwargs['username']

    def handle_no_permission(self):
        raise Http404

    def get_object(self):
        return Profile.objects.filter(user=self.request.user).first()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
