$( document ).ready(function(){
    $('#comment-form').on('submit', function (e) {
        e.preventDefault();
        createComment();
    });

    var createComment = function() {
        $.post($('#comment-form').attr('action'), {
            comment: $('#id_comment').val(),
            name: $('#id_name').val(),
            object_url: location.pathname
        }).done(function(data) {
            if (!data.error) {
                $.get(data.url, function(data) {
                    $(data).insertAfter('#new-comment');
                    $('#id_comment').val('');
                });
            }
        });
    };
});
