$( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('select:not([multiple])').material_select();
    $('.materialboxed').materialbox();
    $('select.browser-default').select2();
    $('select[multiple]').select2({
        tags: true,
        tokenSeparators: [';', ',', ' ']});
    $(':required').blur(function() {
        if ($(this).val() === '') { $(this).addClass('invalid'); }
    });
    $('input[type="url"]').focusout(function() {
        var url = $(this).val();
        if (url && url.slice(0,10).indexOf('://') === -1) {
            $(this).val('http://' + url);
        }
    });


    // Backgrounds
    var replace_bg = function(match, indent, placeholder) {
        return indent + 'background-image: url(' + $('#background').text() + ');';
    };
    $("style.background").text(
        $("style.background").text().replace(/(.*)(BACKGROUND-IMAGE)/g, replace_bg)
    );
    $('#volunteers').css('background-image', 'url('+ $('.background', '#volunteers').text() +')');

    // Categories of home page
    $('.category').click(function () {
        $(this).toggleClass('selected');
        $('[value="'+ $(this).attr('id') +'"]', '#id_cat').prop(
            'checked', $(this).hasClass('selected')
        );
    });
    $(':checkbox', '#id_cat').each(function () {
        $('#' + $(this).attr('value'), '#categories')
            .toggleClass('selected', $(this).prop('checked'));
    });

    // City autocomplete
    $.getJSON($('#city-autocomplete-url').text(), function(data) {
        $('#city-field').autocomplete({
            lookup: data.suggestions,
            onSelect: function (suggestion) {
                $('#id_city').val(suggestion.data);
            }
        });
    });


    // Dropdown
    $('#filters-toggle').click(function(e) {
        $('#filters-dropdown').slideToggle();
        $(this).toggleClass('active');
        e.preventDefault();
    });


    // Educational loading on Home page
    var loadPost = function(container, url) {
        $.get(url, function(data) {
            container.append($(data));
        });
    };
    var loadEducationalPosts = function(page, postType, topic) {
        $ul = $('#posts-ul', '#learn');
        url = new URI($ul.data('url'));
        url.addSearch('page', page);
        if (postType !== undefined) {
            url.addSearch('post-type', postType);
        }
        if (topic !== undefined) {
            url.addSearch('topic', topic);
        }
        $.getJSON(url, function (data) {
            data.posts.forEach(function(post) {
                loadPost($ul, post);
            });
            if (data.more) { $('#more').show(); }
            else { $('#more').hide(); }
        });
        nextPage = page + 1;  // Global variables
        postType = postType;
    };
    $('#more').click(function(e) {
        loadEducationalPosts(nextPage, postType);
        e.preventDefault();
    });
    $("[id^='post-type-']").click(function(e) {
        $('#posts-ul', '#learn').empty();
        var wasSelected = $(this).hasClass('selected');
        $('a.post-type-selector').removeClass('selected');
        $(this).toggleClass('selected', !wasSelected);
        postType = $(this).attr('id').replace('post-type-', '');
        if (wasSelected) {
            postType = undefined;
        }
        loadEducationalPosts(1, postType);
        e.preventDefault();
    });
    $('select', '#learn').change(function(e) {
        $('a.post-type-selector').removeClass('selected');
        topic = e.target.value;
        if (topic === 'topics') {
            topic = undefined;
        }
        $('#posts-ul', '#learn').empty();
        loadEducationalPosts(1, undefined, topic);
    });
    if ($('#posts-ul').length > 0) {
        $('select', '#learn').val('topics');
        postType = undefined;
        loadEducationalPosts(1, postType);
    }


    // Educational Post form
    var switchPostType = function(select) {
        var postType = select.val();
        if (postType !== '') {
            $('.post-type').hide();
            $('.' + postType).show();
        }
    };
    $('#id_post_type').change(function(e) {
        switchPostType($(this));
    });
    switchPostType($('#id_post_type'));

    $('#article-body').find("a").attr("target", "_blank");


    // Language switcher
    $('li:not(.disabled)', '#language-switcher').click(function() {
        $(this).closest('form').submit();
    })
});
