$(document).ready(function(){
    $(document).on('click', '.rating-star', function(e) {
        e.preventDefault();
        createRating($(this).data('rating'));
    });

    var createRating = function(rating) {
        $.post($('#rating-form').attr('action'), {
            place: $('#id_place').val(),
            rating: rating
        }).done(function(data) {
            if (!data.error) {
                $('#stars').load(data.url);
                Materialize.toast("Thanks for rating!", 3000);
            }
        }).fail(function(data) {
            if (data.status === 403) {
                Materialize.toast("You must be logged to rate", 3000);
            }
        });
    };
});
