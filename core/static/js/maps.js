$( document ).ready(function(){
    // https://myprojects.geoapify.com/api/fPFUHORbJP3ATHJO2dOH/keys:
    var apiKey = '18f4319572964b62b5e1329b77f584c3'
    var staticUrl = '/static/img/ecomarkers/';
    var Ecomarker = L.Icon.extend({
        options: {
            shadowUrl: staticUrl+'marker-shadow.png',
            iconSize:     [29, 41],
            shadowSize:   [41, 41],
            iconAnchor:   [15, 41],
            shadowAnchor: [14, 41],
            popupAnchor:  [0, -41]
        }
    });
    var ecomarkers = {
        green: new Ecomarker({iconUrl: staticUrl+'green.png'}),
        brown: new Ecomarker({iconUrl: staticUrl+'brown.png'}),
        magenta: new Ecomarker({iconUrl: staticUrl+'magenta.png'}),
        blue: new Ecomarker({iconUrl: staticUrl+'blue.png'}),
        yellow: new Ecomarker({iconUrl: staticUrl+'yellow.png'}),
        dark: new Ecomarker({iconUrl: staticUrl+'dark.png'})
    };


    // http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
    var tileLayer = L.tileLayer('https://maps.geoapify.com/v1/tile/klokantech-basic/{z}/{x}/{y}.png?apiKey={apiKey}', {
        attribution: 'Powered by <a href="https://www.geoapify.com/" target="_blank">Geoapify</a> | © OpenStreetMap <a href="https://www.openstreetmap.org/copyright" target="_blank">contributors</a>',        maxZoom: 18,
        apiKey: apiKey
    });

    var removeFeatures = function() {
        if (typeof features !== 'undefined') {
            map.removeLayer(features);
        }
    };

    var noPlaceFound = function(map) {
        $.getJSON($('#geojson-bbox-url').text(), function(data) {
            Materialize.toast($("#no-place-found").text(), 2000);
            removeFeatures();
            var bounds = L.latLngBounds(data.southwest, data.northeast);
            map.fitBounds(bounds);
        });
    };

    if ($('#map').length) {
        var map = L.map('map');

        onEachFeature = function(feature, layer) {
            if (feature.properties && feature.properties.popupContent) {
                layer.bindPopup(feature.properties.popupContent);
            }
        };

        pointToLayer = function(feature, latlng) {
            return L.marker(latlng,
                {icon: ecomarkers[feature.properties.color]});
        };

        $.getJSON($('#geojson-url').text(), function(data) {
            if (data.features.length) {
                features = L.geoJson(data, {
                    onEachFeature: onEachFeature,
                    pointToLayer: pointToLayer
                });
                map.fitBounds(features.addTo(map).getBounds());
            } else { noPlaceFound(map); }
        });

        tileLayer.addTo(map);


        // FILTERS
        // Fold subcategories at loading
        $('.subcategory-list').hide();

        // Unfold if in URL at loading
        var url = new URI();
        var query = URI.parseQuery(url.search());
        var cats = (typeof(query.cat) === 'string') ? [query.cat]: query.cat;
        if (cats !== undefined) {
            cats.forEach(function(cat) {
                $('#'+cat).closest('.subcategory-list').slideDown();
                $('#'+cat).closest('.category').addClass('active');
            });
        }

        // Utility function to toggle Subcategory in URL and active state
        var toggleSubcategory = function($cat, url, state) {
            var activate = (state !== undefined) ? state : $cat.parent().hasClass('active');
            if (activate) {
                url.addSearch('cat', $cat.attr('id'));
            } else {
                url.removeSearch('cat', $cat.attr('id'));
            }
            $cat.parent().toggleClass('active', activate);
        };

        // Do stuff when Category or Subcategory is clicked
        $('a', '#filters-dropdown').click(function(e) {
            e.preventDefault();
            var url = new URI();
            // UI
            var isActive = $(this).parent().hasClass('active');
            if ($(this).hasClass('subcategory-link')) {
                toggleSubcategory($(this), url, !isActive);
            } else if ($(this).hasClass('category-link')) {
                $('.subcategory-link', $(this).next()).each(function() {
                    toggleSubcategory($(this), url, !isActive);
                });
                $(this).next().slideToggle(!isActive);
                $(this).parent().toggleClass('active', !isActive);
            }

            // Getting the data
            window.history.pushState({}, '', url);
            uri = url.clone();
            uri.directory(uri.directory().replace('/map/', '/api/geojson/'));
            $.getJSON(uri, function(data) {
                if (data.features.length) {
                    removeFeatures();
                    features = L.geoJson(data, {
                        onEachFeature: onEachFeature,
                        pointToLayer: pointToLayer
                    });
                    map.fitBounds(features.addTo(map).getBounds());
                } else { noPlaceFound(map, data); }
            });
        });
    }

    if ($('#edit-map').length) {
        var updateLatLng = function(lat, lng) {
            $('#id_lat').attr('value', lat);
            $('#id_lng').attr('value', lng);
        };

        var editMap = L.map('edit-map').setView([50, 14], 4);
        tileLayer.addTo(editMap);

        var drawnItems = new L.FeatureGroup();
        editMap.addLayer(drawnItems);

        var drawControl = new L.Control.Draw({
            position: 'topright',
            draw: {
                polyline: false,
                polygon: false,
                circle: false,
                rectangle: false,
                marker: {icon: ecomarkers.dark}
            },
            edit: {
                featureGroup: drawnItems
            }
        });
        editMap.addControl(drawControl);

        editMap.on('draw:created', function (e) {
            var marker = e.layer;
            updateLatLng(marker._latlng.lat, marker._latlng.lng);

            drawnItems.addLayer(marker);
            editMap.setView(marker._latlng, 18);
            $('.leaflet-draw-section:first-of-type').hide();
        });

        editMap.on('draw:edited', function (e) {
            var layers = e.layers._layers;
            if (Object.keys(layers).length) {
                var marker = layers[Object.keys(layers)[0]];
                updateLatLng(marker._latlng.lat, marker._latlng.lng);
                editMap.setView(marker._latlng, 18);
            }
        });

        editMap.on('draw:deleted', function () {
            updateLatLng('', '');
            $('.leaflet-draw-section:first-of-type').show();
        });

        // Edit Mode: creates a marker from fields and add to the edit map
        var lat = $('#id_lat').attr('value'),
            lng = $('#id_lng').attr('value');
        if (lat || lng) {
            var marker = new L.Draw.Marker(editMap, {icon: ecomarkers.dark});
            marker._marker = L.marker([lat, lng]);
            marker._fireCreatedEvent();
        }

        // Zoom to City bounds if no initial data
        if (Object.keys(drawnItems._layers).length === 0) {
            $.getJSON($('#bbox-url').text(), function(data) {
                editMap.fitBounds([data.southwest, data.northeast]);
            });
        }
    }

});
