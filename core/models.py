from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from django_countries.fields import CountryField


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True)
    country = CountryField(verbose_name=_("country"), blank_label=_("Country"), blank=True)
    city = models.ForeignKey('maps.City', verbose_name=_("city"), related_name='profiles', null=True, blank=True)
    avatar = models.ImageField(_("avatar"), upload_to='avatars', blank=True)
    description = models.TextField(_("about me"), blank=True)
    why_i_joined = models.TextField(_("I joined ECOmaps because:"), blank=True)

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    def __str__(self):
        return self.user.get_full_name() or self.user.username

    def get_absolute_url(self):
        return reverse('profile', kwargs={'username': self.user.username})
