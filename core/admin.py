from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _

from .models import Profile

from registration.models import RegistrationProfile



class ProfileInline(admin.TabularInline):
    model = Profile
    can_delete = False


class UserAdmin(UserAdmin):
    inlines = (ProfileInline,)
    list_display = (
        'username', 'email', 'first_name', 'last_name',
        'last_login', 'date_joined',
        'is_active', 'is_staff', 'is_superuser',
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser'),
            'classes': ['collapse']}),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined'),
            'classes': ['collapse']}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
    )
    actions = ['activate', 'inactivate']

    def activate(self, request, queryset):
        rows_updated = queryset.update(is_active=True)
        self.message_user(request, "%s users activated." % rows_updated)
    activate.short_description = _("Activate selected users")

    def inactivate(self, request, queryset):
        rows_updated = queryset.update(is_active=False)
        self.message_user(request, "%s users inactivated." % rows_updated)
    inactivate.short_description = _("Inactivate selected users")

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass


admin.site.unregister(Site)
admin.site.unregister(Group)
admin.site.unregister(RegistrationProfile)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
