# Migrate data from Taggit to aps.Tag
from collections import namedtuple
import json

from maps.models import Tag, Place


with open('dump_taggit.json', 'r') as file:
    data = json.load(file, object_hook=lambda d: namedtuple('Taggit', d.keys())(*d.values()))

# Taggit(model='taggit.tag', fields=Taggit(name='local', slug='local'), pk=11)
tags = {obj.pk: dict(zip(['name', 'slug'], obj.fields)) for obj in data if obj.model == 'taggit.tag'}

for pk, fields in tags.items():
    tag = Tag(pk=pk, name=fields['name'], slug=fields['slug'])
    tag.save()


# Taggit(model='taggit.taggeditem', fields=Taggit(content_type=7, tag=21, object_id=8), pk=37)
taggeditems = [(obj.fields.tag, obj.fields.object_id) for obj in data if obj.model == 'taggit.taggeditem']
ignored_places = []

for tag_id, place_id in taggeditems:
    print(tag_id, place_id)
    try:
        place = Place.objects.get(id=place_id)
    except Place.DoesNotExist:
        ignored_places.append(place_id)
        continue
    place.tags.add(Tag.objects.get(id=tag_id))

print(ignored_places)
