from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Post, Topic


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['__str__', 'title', 'subtitle', 'post_type', 'allow_comments', 'created']
    list_editable = ['title', 'subtitle']
    date_hierarchy = 'created'
    list_filter = ['created', 'post_type', 'allow_comments']

    fieldsets = (
        (_("Informations on the Home page"), {
            'fields': (
                'post_type',
                ('title', 'slug'),
                'subtitle',
                'preview',
                'topics',
            ),
        }),
        (_("Infographic or Video"), {
            'classes': ('collapse', 'wide'),
            'fields': ('infographic', 'url', 'caption'),
        }),
        (_("Article"), {
            'classes': ('collapse',),
            'fields': ('picture', 'abstract', 'body',),
        }),
        (_("Comments"), {
            'fields': ('allow_comments',)
        }),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.modified_by = request.user
        obj.save()
