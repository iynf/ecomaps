from urllib.parse import urlparse, parse_qs

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.text import slugify, capfirst
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.models import TimeStampedModel

from .managers import TopicManager


class Topic(models.Model):
    name = models.CharField(_("name"), unique=True, max_length=100)
    slug = models.SlugField(_("slug"), unique=True)

    objects = TopicManager()

    class Meta:
        verbose_name = _("Topic")
        verbose_name_plural = _("Topics")

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = capfirst(self.name)
        return super().save(*args, **kwargs)


class Post(TimeStampedModel):
    INFOGRAPHIC = 'infographic'
    VIDEO = 'video'
    ARTICLE = 'article'
    EVENT = 'event'
    ARTICLE_OR_EVENT = ('article', 'event')
    POST_TYPES = (
        (INFOGRAPHIC, _("Infographic")),
        (VIDEO, _("Video")),
        (ARTICLE, _("Article")),
        (EVENT, _("Event")),
    )
    POST_TYPES_PLURAL = (
        (INFOGRAPHIC, _("Infographics")),
        (VIDEO, _("Videos")),
        (ARTICLE, _("Articles")),
        (EVENT, _("Events")),
    )
    title = models.CharField(_("title"), max_length=200)
    slug = models.SlugField(unique=True, max_length=200, help_text=_("For the URL: http://ecomaps.eu/en/learn/here-is-the-slug"))
    subtitle = models.CharField(_("subtitle"), max_length=200, blank=True)
    post_type = models.CharField(_("post type"), max_length=20, choices=POST_TYPES)
    preview = models.ImageField(_("preview"), upload_to='educational/previews',
                                help_text=_("Small image for the home page. Better if 300x250 px."))
    infographic = models.ImageField(_("infographic"), upload_to='educational/infographics', blank=True)
    picture = models.ImageField(_("picture"), upload_to='educational/pictures', blank=True)
    caption = models.TextField(_("caption"), blank=True)
    url = models.URLField(_("video URL"), blank=True)
    abstract = models.TextField(_("abstract"), blank=True)
    body = models.TextField(_("body"), blank=True)
    topics = models.ManyToManyField('educational.Topic', verbose_name=_("topics"))
    allow_comments = models.BooleanField(_("allow comments"), default=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='post_created')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='post_modified')

    class Meta:
        verbose_name = _("Educational Post")
        verbose_name_plural = _("Educational Posts")

    def __str__(self):
        return self.title

    @property
    def youtube_id(self):
        """
        Examples:
        - http://youtu.be/SA2iWivDJiE
        - http://www.youtube.com/watch?v=_oPAwA_Udwc&feature=feedu
        - http://www.youtube.com/embed/SA2iWivDJiE
        - http://www.youtube.com/v/SA2iWivDJiE?version=3&amp;hl=en_US
        """
        query = urlparse(self.url)
        if query.hostname == 'youtu.be':
            return query.path[1:]
        if query.hostname.endswith('youtube.com'):
            if query.path == '/watch':
                p = parse_qs(query.query)
                return p['v'][0]
            if query.path.startswith('/embed/'):
                return query.path.split('/')[2]
            if query.path.startswith('/v/'):
                return query.path.split('/')[2]
            if query.path == '/':
                p = parse_qs(query.fragment)
                return p['/watch?v'][0]
        return None

    @property
    def youtube_embed(self):
        return 'https://www.youtube.com/embed/' + self.youtube_id

    def get_url(self, url_name='post', no_kwargs=False):
        kwargs = {} if no_kwargs else {'slug': self.slug}
        return reverse(url_name, kwargs=kwargs)

    def get_absolute_url(self):
        return self.get_url()

    def get_update_url(self):
        return self.get_url('post_update')

    def get_preview_url(self):
        return self.get_url('post_preview')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)
