from django import forms

from ckeditor.widgets import CKEditorWidget

from .models import Post, Topic
from .form_fields import TopicField


class PostForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget)
    topics = TopicField(queryset=Topic.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if field.widget.__class__.__name__ == 'Textarea':
                field.widget.attrs['class'] = ' materialize-textarea'

    class Meta:
        model = Post
        fields = (
            'post_type',
            'title',
            'subtitle',
            'body',
            'preview',
            'infographic',
            'picture',
            'caption',
            'url',
            'abstract',
            'topics',
        )

    def save(self, commit=True):
        post = super().save(commit=False)
        if getattr(post, 'created_by', None) is None:
            post.created_by = self.request.user
        post.modified_by = self.request.user
        if commit:
            post.save()
            self.save_m2m()
        return post
