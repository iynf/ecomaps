from django.db import models


class TopicManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            popularity=models.Count('post')
        ).order_by('-popularity', 'name')

    def used(self):
        return self.get_queryset().filter(popularity__gt=0)

    def unused(self):
        return self.get_queryset().filter(popularity=0)
