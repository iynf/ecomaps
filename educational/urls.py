from django.conf.urls import url

from .views import PostDetail, PostCreate, PostUpdate, PostPreview, JsonLastsPosts


urlpatterns = [
    url(r'^new/$', PostCreate.as_view(), name='post_new'),
    url(r'^preview/(?P<slug>[\w-]+)/$', PostPreview.as_view(), name='post_preview'),
    url(r'^api/post/lasts/$', JsonLastsPosts.as_view(), name='lasts_posts'),
    url(r'^(?P<slug>[\w-]+)/$', PostDetail.as_view(), name='post'),
    url(r'^(?P<slug>[\w-]+)/edit/$', PostUpdate.as_view(), name='post_update'),
]
