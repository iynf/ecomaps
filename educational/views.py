from django.views import generic
from django.contrib.auth.mixins import PermissionRequiredMixin

from core.mixins import JsonResponseMixin, RequestMixin
from .models import Post
from .forms import PostForm


class PostDetail(generic.DetailView):
    model = Post


class PostCreate(PermissionRequiredMixin, RequestMixin, generic.CreateView):
    permission_required = 'educational.add_post'
    model = Post
    form_class = PostForm


class PostUpdate(PermissionRequiredMixin, RequestMixin, generic.UpdateView):
    permission_required = 'educational.change_post'
    model = Post
    form_class = PostForm


class PostPreview(generic.DetailView):
    model = Post
    template_name = 'educational/post_preview.html'


class JsonLastsPosts(JsonResponseMixin, generic.ListView):
    model = Post
    paginate_by = 6
    paginate_orphans = 2
    ordering = '-created'

    def dispatch(self, request, *args, **kwargs):
        self.post_type = request.GET.get('post-type')
        self.topic = request.GET.get('topic')
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        if self.post_type:
            qs = qs.filter(post_type=self.post_type)
        if self.topic:
            qs = qs.filter(topics__slug=self.topic)
        return qs

    def get_data(self, context):
        return {
            'posts': [post.get_preview_url() for post in context['object_list']],
            'more': context['page_obj'].has_next(),
        }
