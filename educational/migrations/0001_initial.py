# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django_extensions.db.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(editable=False, default=django.utils.timezone.now, blank=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(editable=False, default=django.utils.timezone.now, blank=True, verbose_name='modified')),
                ('title', models.CharField(verbose_name='title', max_length=200)),
                ('slug', models.SlugField(unique=True)),
                ('subtitle', models.CharField(verbose_name='subtitle', blank=True, max_length=200)),
                ('post_type', models.CharField(verbose_name='type', choices=[('picture', 'Infographics'), ('video', 'Video'), ('article', 'Article')], max_length=10)),
                ('url', models.URLField(verbose_name='video URL', blank=True)),
                ('picture', models.FileField(verbose_name='infographics', upload_to='educational')),
                ('caption', models.TextField(verbose_name='caption', blank=True)),
                ('abstract', models.TextField(verbose_name='abstract', blank=True)),
                ('body', models.TextField(verbose_name='body', blank=True)),
                ('allow_comments', models.BooleanField(verbose_name='allow comments', default=True)),
                ('created_by', models.ForeignKey(related_name='post_created', to=settings.AUTH_USER_MODEL)),
                ('modified_by', models.ForeignKey(related_name='post_modified', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
        ),
    ]
