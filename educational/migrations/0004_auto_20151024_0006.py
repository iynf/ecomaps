# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('educational', '0003_post_preview'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='infographic',
            field=models.ImageField(blank=True, verbose_name='infographic', upload_to='educational/infographics'),
        ),
        migrations.AlterField(
            model_name='post',
            name='picture',
            field=models.ImageField(blank=True, verbose_name='picture', upload_to='educational/pictures'),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_type',
            field=models.CharField(verbose_name='type', choices=[('infographic', 'Infographic'), ('video', 'Video'), ('article', 'Article')], max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='preview',
            field=models.ImageField(verbose_name='preview', upload_to='educational/previews', help_text='300x250 px'),
        ),
    ]
