# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('educational', '0002_auto_20151020_1026'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='preview',
            field=models.ImageField(default='', help_text='300x250 px', verbose_name='preview', upload_to='educational/preview'),
            preserve_default=False,
        ),
    ]
