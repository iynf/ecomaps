from django.utils.text import capfirst, slugify
from django.forms.models import ModelMultipleChoiceField
from django.core.exceptions import ValidationError

from .models import Topic


class TopicField(ModelMultipleChoiceField):
    def _check_values(self, value):
        """
        Given a list of possible PK values, returns a QuerySet of the
        corresponding objects. Raises a ValidationError if a given value is
        invalid (not a valid PK, not in the queryset, etc.)
        """
        key = self.to_field_name or 'pk'
        # deduplicate given values to avoid creating many querysets or
        # requiring the database backend deduplicate efficiently.
        try:
            value = set(value)
        except TypeError:
            # list of lists isn't hashable, for example
            raise ValidationError(
                self.error_messages['list'],
                code='list',
            )
        updated_value = set(value)
        for pk in value:
            try:
                self.queryset.filter(**{key: pk})
            except (ValueError, TypeError):
                topic, new = Topic.objects.get_or_create(
                    slug=slugify(pk),
                    name=capfirst(pk))
                updated_value.remove(pk)
                updated_value.add(str(topic.pk))
        value = updated_value
        qs = Topic.objects.filter(**{'%s__in' % key: value})
        return qs
